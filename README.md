# Wallet Stone

Desafio Stone que consiste em criar uma carteira virtual de criptomoedas.

## Ferramentas Utilizadas

* Xcode 9.3
* Swift 4.1
* CoreData
* CocoaPods

## Authors

**André Souza** - [Linkedin](https://www.linkedin.com/in/andrehsouza/)
